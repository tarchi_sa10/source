$(function () {
    //フォーム指定
  $('form').validate({

    //検証ルール設定
    rules: {
      project_id: {
        required: true
      }
    },

    //エラーメッセージ設定
    messages: {
      project_id: {
        required: '入力必須です'
      }
    },

    //エラーメッセージ出力箇所設定
    errorPlacement: function(error, element){
      error.insertAfter(element);
      //error.insertBefore(element);
    }
  });
});
