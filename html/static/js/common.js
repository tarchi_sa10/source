  $(function () {
    //■Project項目
      //■Ligand-lot項目
      let trClassName = '.prj-area';//コピー行のクラス名
      let delBtnClassName = '.prj-del-btn';//削除ボタンのクラス名
      let addBtnClassName = '.prj-add-btn';//追加ボタンのクラス名
      let txtClassName = '.prj-id';//最後の削除ボタンで消去するinputのクラス名
      let txtClassName2 = '.prj-lgd-cd';//最後の削除ボタンで消去するinputのクラス名
      //追加ボタンで追加するタグ
      var newHtml = '<tr class="prj-area"><th>Project</th><td><input type="text" class="prj-id" name="project_id"></td><td>(<input type="text" class="prj-lgd-cd" name="ligand_code">)<button type="button" class="prj-del-btn" id="del-project">-</button><button type="button" class="prj-add-btn" id="add-project">+</button></td><tr>';
    //入力項目追加
    $(document).on('click', addBtnClassName,function(e){
      let countTr = $(trClassName).length;
      let clickRow = $(this).closest(trClassName);
      clickRow.after(newHtml);
      $(addBtnClassName).slice(0,countTr).hide();
      //$(delBtnClassName).slice(0,countTr).hide();
    });

    //入力項目削除
    $(document).on('click', delBtnClassName,function(e){
        let countTr = $(trClassName).length;
        let clickRow = $(this).closest(trClassName);
        let clickBtnIndex = $(delBtnClassName).index(this);
        let countClickBtn = $(delBtnClassName).length;
      if (countTr < 2) {//項目が一つの場合は項目削除しない
        //テキストボックスの値を削除
        clickRow.find(txtClassName).val('');
      } else if (countClickBtn == clickBtnIndex + 1) {
        clickRow.prev().find(delBtnClassName).show();
        clickRow.prev().find(addBtnClassName).show();
        clickRow.remove();
      } else{
        clickRow.prev().find(delBtnClassName).show();
        //clickRow.prev().find(addBtnClassName).show();
        clickRow.remove();
      }
     });
  });

  $(function () {
      //■Ligand-lot項目
      let trClassName = '.lgd-lot-area';//コピー行のクラス名
      let delBtnClassName = '.lgd-del-btn';//削除ボタンのクラス名
      let addBtnClassName = '.lgd-add-btn';//追加ボタンのクラス名
      let txtClassName = '.lgd-lot-name';//最後の削除ボタンで消去するinputのクラス名
      //追加ボタンで追加するタグ
      let newHtml = '<tr class="lgd-lot-area"><th>Lots</th><td colspan="2"><input type="text" class="lgd-lot-name" name="ligand_lot_name"><button type="button" class="lgd-del-btn">-</button><button type="button" class="lgd-add-btn">+</button></td></tr>';
    //入力項目追加
    $(document).on('click', addBtnClassName,function(e){
      let countTr = $(trClassName).length;
      let clickRow = $(this).closest(trClassName);
      clickRow.after(newHtml);
      //$(delBtnClassName).slice(0,countTr).hide();
      $(addBtnClassName).slice(0,countTr).hide();
    });

    //入力項目削除
    $(document).on('click', delBtnClassName,function(e){
        let countTr = $(trClassName).length;
        //let clickRow = $(this).parent().parent();
        let clickRow = $(this).closest(trClassName);
        //let clickIndex = $(this).index();
        let clickBtnIndex = $(delBtnClassName).index(this);
        let countClickBtn = $(delBtnClassName).length;
      if (countTr < 2) {//項目が一つの場合は項目削除しない
        //テキストボックスの値を削除
        clickRow.find(txtClassName).val('');
      } else if (countClickBtn == clickBtnIndex + 1) {
        clickRow.prev().find(delBtnClassName).show();
        clickRow.prev().find(addBtnClassName).show();
        clickRow.remove();
      } else{
        clickRow.prev().find(delBtnClassName).show();
        //clickRow.prev().find(addBtnClassName).show();
        clickRow.remove();
      }
    });
  });
