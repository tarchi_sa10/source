#encoding-utf8
import json

def row2dict(row):
    dict = {}
    for column in row.__table__.columns:
        dict[column.name] = str(getattr(row, column.name))

    return dict

def table2list(table):
    return [row2dict(row) for row in table]

def table2json(table):
    return [json.dumps(row2dict(row)) for row in table]
