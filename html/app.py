# -*- coding: utf-8 -*-
from common import common
from flask import Flask, flash, redirect, render_template, \
     request, url_for
from flask import jsonify
import json
from flask import session
from werkzeug.utils import secure_filename
import os
from sqlalchemy.sql import func
from datetime import datetime
#設定ファイルの読み込み
from config import *
# DB setting and models
from setting import connection
import models
#テスト用
#ランダムな整数を生成するため
import random
#ログ
import logging
#画面毎の機能
import control
import pandas as pd

# ログレベルを DEBUG に変更
#logging.basicConfig(
#    filename = LOGGING_FILE + datetime.now().strftime('_%Y%m%d_%H%M%S'),
#    level = logging.DEBUG,
#    format = '[%(asctime)s] %(name)s %(levelname)s: %(message)s',
#    datefmt = '%Y-%m-%d %H:%M:%S')

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)

#ログイン機能を付けるまでユーザーIDを固定する
TEMP_USER_ID = "12345678"

#configに書き出す
REQUIRE_LOGIN = ['/lilgand/edit/view', '/ligand/edit/save',
                '/ligand/new/view', '/ligand/new/save']
'''
# 各route関数の前に実行される処理
@app.before_request
def before_request():
    #redirect(request.referrer + '#signinModal')
    # 静的リソースへのアクセスについてはチェックしない
    if request.path.startswith('/static/'):
        return
    # セッションにusernameが保存されている．つまりログイン済み
    if Session.get('username') is not None:
        return
    # リクエストパスがログインを要すページでない場合
    if request.path not in REQUIRE_LOGIN:
        return
    # ログインされておらず，ログインページに関するリクエストでない場合
    return render_template('login.html') #redirect(request.referrer)
'''

# ログイン処理を行う
@app.route('/login', methods=['GET', 'POST'])
def login():
    # ログイン処理
    if request.method == 'POST' and _is_account_valid():
        # セッションにユーザ名を保存してからトップページにリダイレクト
        Session['username'] = request.form['username']
        return redirect(request.path)
    # ログインページに戻る
    return render_template('login.html')


# 個人認証を行い，正規のアカウントか確認する
def _is_account_valid():
    username = request.form.get('username')
    # この例では，ユーザ名にadminが指定されていれば正規のアカウントであるとみなしている
    # ここで具体的な個人認証処理を行う．認証に成功であればTrueを返すようにする
    if username == 'admin':
        return True
    return False


# ログアウト処理を行う
@app.route('/logout')
def logout():
    # セッションからusernameを取り出す
    Session.pop('username', None)
    return redirect(url_for('login'))


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/postText', methods=['POST'])
def lower_conversion():
    text = request.json['text']
    if "ping" in text:
        return_data = {"result":"pong"}
        return jsonify(ResultSet=json.dumps(return_data))
    lower_text = text.lower()
    return_data = {"result":lower_text}
    return jsonify(ResultSet=json.dumps(return_data))



'''
@app.route('/')
def index():
    return render_template('index.html')
'''
@app.route('/ligand/list', methods=['GET','POST'])
#@app.route('/<Pyファイル名>/<画面名>/<機能名>')
def ligand_list_view():
    return control.ligand.List.view()
#exec("control." <Pyファイル名>+ "." + <画面名>　+ "." +)

@app.route('/ligand/details', methods=['GET','POST'])
def ligand_details_view():
    return control.ligand.Details.view()

@app.route('/ligand/edit/view', methods=['GET','POST'])
def ligand_edit_view():
    return control.ligand.Edit.view()

@app.route('/ligand/edit/save', methods=['GET','POST'])
def ligand_edit_save():
    return control.ligand.Edit.save()

@app.route('/ligand/edit/delete', methods=['GET','POST'])
def ligand_edit_delete():
    return control.ligand.Edit.delete()

@app.route('/ligand/new/view', methods=['GET','POST'])
def ligand_new_view():
    return control.ligand.New.view()

@app.route('/ligand/new/save', methods=['GET','POST'])
def ligand_new_save():
    return control.ligand.New.save()

if __name__ == '__main__':
    app.debug = True

    app.run(port=8000)
