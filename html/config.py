import os

# 定数の定義
# postgresqlのDBの設定
DATABASE = 'postgresql'
USER = 'postgres'
PASSWORD = 'postgres'
HOST = '192.168.56.102'
PORT = '5432'
DB_NAME = 'pipeline'

# ログの出力先
LOGGING_FILE = '/logfile/logger.log'

# attached fileの格納先
ATTACHED_FILE_DIRECTORY = './attached_file/'

# ligand fileの格納先
LIGAND_FILE_DIRECTORY = './ligand/'
