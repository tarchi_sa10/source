import sys
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime,Text, Boolean, Integer
from setting import Base
from setting import ENGINE
from datetime import datetime


class User(Base):
    """
    users table model class
    """
    __tablename__ = 'users'
    user_id = Column('user_id', String(8), primary_key = True)
    user_name = Column('user_name', String(64))
    password = Column('password', String(64))
    mail = Column('mail', String(64))
    create_dtm = Column('create_dtm', DateTime, default = datetime.now)
    create_user_id = Column('create_user_id', String(8))
    last_update_dtm = Column('last_update_dtm', DateTime, default = datetime.now)
    last_update_user_id = Column('last_update_user_id', String(8))

def main(args):
    """
    main func
    """
    Base.metadata.create_all(bind=ENGINE)

if __name__ == "__main__":
    main(sys.argv)
