import sys
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime, Text, Boolean, Integer
from setting import Base
from setting import ENGINE
from datetime import datetime


class Project(Base):
    """
    project table model class
    """
    __tablename__ = 'project'

    # member object
    project_id = Column('project_id', String(10), primary_key = True)
    project_name = Column('project_name', String(10), primary_key = True)
    modality_cd = Column('modality_cd', String(2))
    terminated_flg = Column('terminated_flg', Boolean)
    remark_txt = Column('remark_txt', Text)
    create_dtm = Column('create_dtm', DateTime, default = datetime.now)
    create_user_id = Column('create_user_id', String(8))
    last_update_dtm = Column('last_update_dtm', DateTime, default = datetime.now)
    last_update_user_id = Column('last_update_user_id', String(8))



def main(args):
    """
    main func
    """
    Base.metadata.create_all(bind=ENGINE)

if __name__ == "__main__":
    main(sys.argv)
