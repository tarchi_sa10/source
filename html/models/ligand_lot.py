import sys
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime, Text, Boolean, Integer
from setting import Base
from setting import ENGINE
from datetime import datetime


class LigandLot(Base):
    """
    ligand_lot table model class
    """
    __tablename__ = 'ligand_lot'

    # member object
    ligand_lot_id = Column('ligand_lot_id', String(10), primary_key = True)
    ligand_id = Column('ligand_id', String(10), primary_key = True)
    ligand_lot_name = Column('ligand_lot_name', String(128))
    create_dtm = Column('create_dtm', DateTime, default = datetime.now)
    create_user_id = Column('create_user_id', String(8))
    last_update_dtm = Column('last_update_dtm', DateTime, default = datetime.now)
    last_update_user_id = Column('last_update_user_id', String(8))


def main(args):
    """
    main func
    """
    Base.metadata.create_all(bind=ENGINE)

if __name__ == "__main__":
    main(sys.argv)
