import sys
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime, Text, Boolean, Integer
from setting import Base
from setting import ENGINE
from datetime import datetime


class LigandProject(Base):
    """
    ligand_project table model class
    """
    __tablename__ = 'ligand_project'

    # member object
    ligand_id = Column('ligand_id', String(10), primary_key = True)
    project_id = Column('project_id', String(10), primary_key = True)
    ligand_code = Column('ligand_code', String(128))
    create_dtm = Column('create_dtm', DateTime, default = datetime.now)
    create_user_id = Column('create_user_id', String(8))
    last_update_dtm = Column('last_update_dtm', DateTime, default = datetime.now)
    last_update_user_id = Column('last_update_user_id', String(8))


def main(args):
    """
    main func
    """
    Base.metadata.create_all(bind=ENGINE)

if __name__ == "__main__":
    main(sys.argv)
