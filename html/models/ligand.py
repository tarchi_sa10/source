import sys
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime, Text, Boolean, Integer
from setting import Base
from setting import ENGINE
from datetime import datetime


class Ligand(Base):
    """
    ligand table model class
    """
    __tablename__ = 'ligand'

    # member object
    ligand_id = Column('ligand_id', String(10), primary_key = True)
    ligand_name = Column('ligand_name', String(128))
    ligand_aliases_txt = Column('ligand_aliases_txt', Text(64))
    ligand_type_cd = Column('ligand_type_cd', String(2))
    residue_name = Column('residue_name', String(64))
    image_file = Column('image_file', String(64))
    create_dtm = Column('create_dtm', DateTime, default = datetime.now)
    create_user_id = Column('create_user_id', String(8))
    last_update_dtm = Column('last_update_dtm', DateTime, default = datetime.now)
    last_update_user_id = Column('last_update_user_id', String(8))

def main(args):
    """
    main func
    """
    Base.metadata.create_all(bind=ENGINE)

if __name__ == "__main__":
    main(sys.argv)
