# 不要
from flask import Flask

from database import initialize_database
from models import User
import auth as auth_blueprint
import main as main_blueprint


def create_app():
    app = Flask(__name__)
    app.config.from_object('config.Config')
    initialize_database(app)

    app.register_blueprint(auth_blueprint)
    app.register_blueprint(main_blueprint)

    return app


app = create_app()
