
from common import common
from flask import Flask, render_template, request, redirect
from flask import Markup
from werkzeug.utils import secure_filename
import os
from sqlalchemy.sql import func
from datetime import datetime
import pandas as pd

# DB setting and models
from setting import connection
import models

#テスト用
#ランダムな整数を生成するため
import random
TEMP_USER_ID = "12345678"


#画面毎にクラスを定義する
#その中にメソッドを定義する
class List():
    """
    ligandの一覧画面
    """
    def view():
        """
        ligandの一覧画面を表示する
        """
        # コードマスターの参照に関して修正する必要があるかも
        sql = "SELECT ligand.ligand_id, ligand_type_cd, \
                ligand_type_master.code_key_name AS ligand_type_name, \
                ligand_name, ligand_aliases_txt, \
                residue_name, ligand_project.project_id AS project_id, \
                project.project_name AS project_name, ligand_code, \
                ligand_lot_id, ligand_lot_name FROM ligand \
                LEFT JOIN ligand_lot \
                ON ligand.ligand_id = ligand_lot.ligand_id \
                LEFT JOIN ligand_project \
                ON ligand.ligand_id = ligand_project.ligand_id \
                LEFT JOIN project \
                ON ligand_project.project_id = project.project_id \
                LEFT JOIN (SELECT * FROM code_master WHERE code_kind = '0004') AS ligand_type_master\
                ON ligand.ligand_type_cd = ligand_type_master.code_key"
        result = connection.execute(sql)
        data = []
        for row in result:
            ligand_list_dict = dict(row)
            keys = [k for k, v in ligand_list_dict.items() if v is None]
            if len(keys) > 0:
                for key in keys:
                    ligand_list_dict[key] = ""
            data.append(ligand_list_dict)

        #外部結合した結果をリガンドごとにまとめる（project, lot）
        df = pd.DataFrame(data)
        df_ligand = df.drop(["project_id", "project_name", "ligand_code",
                            "ligand_lot_id", "ligand_lot_name"], axis=1).drop_duplicates().set_index('ligand_id')

        # プロジェクト、リガンドコード、ロット名について、リガンドごろに配列を持つ辞書を作る
        project_dict = {"project_name":{k:list(df["project_name"][df["ligand_id"]==k].unique()) for k in df["ligand_id"].unique()}}
        # 同日に辞書にしたい
        ligand_code_dict = {"ligand_code":{k:list(df["ligand_code"][df["ligand_id"]==k].unique()) for k in df["ligand_id"].unique()}}
        lot_name_dict = {"ligand_lot_name":{k:list(df["ligand_lot_name"][df["ligand_id"]==k].unique()) for k in df["ligand_id"].unique()}}
        # 辞書をデータフレームに変換し、リガンドのデータと結合する
        df_ligand_with_project = pd.concat([df_ligand, pd.DataFrame(project_dict)], axis = 1)
        df_ligand_with_project_code = pd.concat([df_ligand_with_project, pd.DataFrame(ligand_code_dict)], axis = 1)
        df_ligand_with_project_code_lot = pd.concat([df_ligand_with_project_code, pd.DataFrame(lot_name_dict)], axis = 1)
        # indexをカラム に戻す
        df_entries = df_ligand_with_project_code_lot.reset_index().rename(columns={'index': 'ligand_id'})
        # Dataframeをjsonに変換する
        entries = df_entries.to_json(orient='records')

        return render_template('ligand/list.html', entries = entries)

class Details():
    """
    ligandの詳細画面
    """

    def view():
        """
        ligandの詳細画面を表示する
        """
        sql = "SELECT ligand.ligand_id, ligand_type_master.code_key_name AS ligand_type_name, ligand_name, ligand_aliases_txt, \
                residue_name, ligand_project.project_id, project_name, \
                ligand_code, ligand_lot_id, ligand_lot_name \
                FROM ligand \
                LEFT JOIN ligand_lot \
                ON ligand.ligand_id = ligand_lot.ligand_id \
                LEFT JOIN ligand_project \
                ON ligand.ligand_id = ligand_project.ligand_id \
                LEFT JOIN project \
                ON ligand_project.project_id = project.project_id \
                LEFT JOIN (SELECT * FROM code_master WHERE code_kind = '0004') AS ligand_type_master\
                ON ligand.ligand_type_cd = ligand_type_master.code_key \
                WHERE ligand.ligand_id = {ligand_id} ".format(
                ligand_id = "'" + request.args.get("ligand_id") + "'")
        result = connection.execute(sql)
        for row in result:
            ligand_dict = dict(row)
            keys = [k for k, v in ligand_dict.items() if v is None]
            if len(keys) > 0:
                for key in keys:
                    ligand_dict[key] = ""
            ligand_details_info = ligand_dict
        # ligand details の\r（改行文字列）を<br/>に置き換える
        ligand_details_info["ligand_aliases_txt"] = Markup(ligand_details_info["ligand_aliases_txt"].replace("\r", "<br/>"))
        return render_template('ligand/details.html', ligand_details_info = ligand_details_info)

class Edit():
    """
    ligandの編集画面
    """

    def view():
        """
        ligandの編集画面を表示する
        """
        sql = "SELECT ligand.ligand_id, ligand_type_cd, ligand_name, ligand_aliases_txt, \
                residue_name, ligand.last_update_dtm AS last_update_dtm, ligand_project.project_id, project_name, \
                ligand_code, ligand_lot_id, ligand_lot_name \
                FROM ligand \
                LEFT JOIN ligand_lot \
                ON ligand.ligand_id = ligand_lot.ligand_id \
                LEFT JOIN ligand_project \
                ON ligand.ligand_id = ligand_project.ligand_id \
                LEFT JOIN project \
                ON ligand_project.project_id = project.project_id \
                WHERE ligand.ligand_id = {ligand_id} ".format(
                ligand_id = "'" + request.args.get("ligand_id") + "'")
        result = connection.execute(sql)
        for row in result:
            ligand_dict = dict(row)
            keys = [k for k, v in ligand_dict.items() if v is None]
            if len(keys) > 0:
                for key in keys:
                    ligand_dict[key] = ""
            ligand_details_info = ligand_dict

        # プルダウンの選択候補
        # リガンドタイプ
        # 修正の必要があるか確認する
        sql = "SELECT code_key, code_key_name\
                FROM code_master WHERE code_kind = '0004'"
        result = connection.execute(sql)
        pulldown_ligand_type = []
        for row in result:
            pulldown_ligand_type.append(dict(row))

        print("********************* pulldown_ligand_type ************************")
        print(pulldown_ligand_type)
        # プロジェクト
        sql = "SELECT project_id, project_name\
                FROM project"
        result = connection.execute(sql)
        pulldown_project = []
        for row in result:
            pulldown_project.append(dict(row))

        return render_template('ligand/edit.html', ligand_details_info = ligand_details_info,
                                pulldown_ligand_type = pulldown_ligand_type,
                                pulldown_project = pulldown_project)

    def save():
        """
        ligandの変更を保存する
        """
        # 楽観的排他処理
        query = connection.query(models.Ligand)
        db_last_update_dtm = query.filter(models.Ligand.ligand_id == request.form["ligand_id"]).first().last_update_dtm
        edid_last_update_dtm = datetime.strptime(request.form["last_update_dtm"], "%Y-%m-%d %H:%M:%S.%f")
        if db_last_update_dtm > edid_last_update_dtm:
            return "更新エラー;編集中にデータ更新されました（{}）".format(db_last_update_dtm) #redirect('/ligand/edit')

        # update
        query = connection.query(models.Ligand)
        ligand_update = query.filter(models.Ligand.ligand_id == request.form["ligand_id"]).first()
        ligand_update.ligand_aliases_txt = request.form["ligand_aliases_txt"]
        ligand_update.ligand_type_cd = request.form["ligand_type_cd"]
        ligand_update.residue_name = request.form["residue_name"]
        ligand_update.image_file = request.form["image_file"]
        ligand_update.last_update_dtm = datetime.now()
        ligand_update.last_update_user_id = TEMP_USER_ID

        #複数行の対応が必要
        query = connection.query(models.LigandProject)
        ligand_project_update = query.filter(
                            models.LigandProject.ligand_id == request.form["ligand_id"],
                            models.LigandProject.project_id == request.form["project_id"]).first()
        if ligand_project_update is None:
            ligand_project_update = models.LigandProject(
                                ligand_id = request.form["ligand_id"],
                                project_id = request.form["project_id"],
                                ligand_code = request.form["ligand_code"],
                                create_user_id = TEMP_USER_ID,
                                last_update_dtm = datetime.now(),
                                last_update_user_id = TEMP_USER_ID)
        else:
            ligand_project_update.ligand_code = request.form["ligand_code"]
            ligand_project_update.last_update_dtm = datetime.now()
            ligand_project_update.last_update_user_id = TEMP_USER_ID

        # lotの数だけ繰り返される
        for update_lot_name in request.form.getlist("ligand_lot_name"):
            # ligand_lotの更新
            query = connection.query(models.LigandLot)
            ligand_lot_update = query.filter(
                                models.LigandLot.ligand_id == request.form["ligand_id"],
                                models.LigandLot.ligand_lot_id == request.form["ligand_lot_id"]).first()

            # 既存のものを変更する場合と新規追加の場合とで処理を変える必要がある
            # ligand_lotが空ではない場合
            if ligand_lot_update is None and update_lot_name != "":
                # idで紐づいているものがない、かつ、ligand_lot_nameの項目が空ではない
                # 新規登録と判断
                sql = "SELECT nextval('ligand_lot_id_seq')"
                res = connection.execute(sql).fetchone()
                new_ligand_lot_id = res["nextval"]
                ligand_lot_update = models.LigandLot(
                                    ligand_lot_id = str(new_ligand_lot_id).zfill(10),
                                    ligand_id = request.form["ligand_id"],
                                    ligand_lot_name = update_lot_name,
                                    create_user_id = TEMP_USER_ID,
                                    last_update_dtm = datetime.now(),
                                    last_update_user_id = TEMP_USER_ID)
                connection.add(ligand_lot_update)
            elif ligand_lot_update is not None and update_lot_name == "":
                # idで紐づくものがある、かつ、入力値が空である
                # 既定値がある入力項目を空にして、削除したと判断
                sql = "DELETE FROM ligand_project \
                    WHERE ligand_id = {ligand_id} AND project_id = {project_id}".format(
                    ligand_id = request.form["ligand_id"], project_id = request.form["project_id"])
                result = connection.execute(sql)
            else:
                ligand_lot_update.ligand_lot_name = update_lot_name
                ligand_lot_update.last_update_dtm = datetime.now()
                ligand_lot_update.last_update_user_id = TEMP_USER_ID
                # Noneの場合、エラーになる可能性がある→未検証
                connection.add(ligand_lot_update)

        connection.add(ligand_update)
        connection.add(ligand_project_update)
        connection.commit()  # ここで登録の処理が実行される
        return redirect('/ligand/list')


    def delete():
        """
        ligandを削除する
        """
        sql = "Delete from ligand \
        where ligand.ligand_id = %s" % str("'" + request.args.get("ligand_id") + "'")
        result = connection.execute(sql)
        connection.commit()

        return redirect('/ligand/list')

class New(object):
    """
    ligandの新規登録画面
    """

    def view():
        """
        ligandの新規登録画面を表示する
        """
        # プルダウンの選択候補
        # リガンドタイプ
        # 修正の必要があるか確認する
        sql = "SELECT code_key, code_key_name\
                FROM code_master WHERE code_kind = '0004'"
        result = connection.execute(sql)
        pulldown_ligand_type = []
        for row in result:
            pulldown_ligand_type.append(dict(row))
        # プロジェクト
        sql = "SELECT project_id, project_name\
                FROM project"
        result = connection.execute(sql)
        pulldown_project = []
        for row in result:
            pulldown_project.append(dict(row))

        return render_template('ligand/new.html',
                                pulldown_ligand_type = pulldown_ligand_type,
                                pulldown_project = pulldown_project)

    def save():
        """
        ligandの新規登録をする
        """
        sql = "SELECT nextval('ligand_id_seq')"
        res = connection.execute(sql).fetchone()
        new_ligand_id = res["nextval"]
        new_ligand = models.Ligand(
                            ligand_id = str(new_ligand_id).zfill(10),
                            ligand_name = request.form["ligand_name"],
                            ligand_aliases_txt = request.form["ligand_aliases_txt"],
                            ligand_type_cd = request.form["ligand_type_cd"],
                            residue_name = request.form["residue_name"],
                            image_file = "" if request.form["image_file"] is None else request.form["image_file"],
                            create_user_id = TEMP_USER_ID,
                            last_update_user_id = TEMP_USER_ID)

        sql = "SELECT nextval('ligand_lot_id_seq')"
        res = connection.execute(sql).fetchone()
        new_ligand_lot_id = res["nextval"]
        new_ligand_lot = models.LigandLot(
                            ligand_lot_id = str(new_ligand_lot_id).zfill(10),
                            ligand_id = str(new_ligand_lot_id).zfill(10),
                            ligand_lot_name = request.form["ligand_lot_name"],
                            create_user_id = TEMP_USER_ID,
                            last_update_user_id = TEMP_USER_ID)

        test_project_id = random.randrange(1000000000)
        new_ligand_project = models.LigandProject(
                            ligand_id = str(new_ligand_id).zfill(10),
                            project_id = str(test_project_id).zfill(10),
                            ligand_code = request.form["ligand_code"],
                            create_user_id = TEMP_USER_ID,
                            last_update_user_id = TEMP_USER_ID)


        connection.add(new_ligand)  # ここではまだDBに登録されていない
        connection.add(new_ligand_lot)  # ここではまだDBに登録されていない
        connection.add(new_ligand_project)  # ここではまだDBに登録されていない
        connection.commit()  # ここで登録の処理が実行される
        return redirect('/ligand/list')
